/*#pragma once
template <size_t N, typename ElemType> class KDTree { 
public:
	KDTree();
	~KDTree();

	size_t dimension() const;
	size_t size() const;
	bool   empty() const;
	void insert(const Point<N>& pt, const ElemType& value);
	bool contains(const Point<N>& pt) const;     
	ElemType& operator[] (const Point<N>& pt);      
	ElemType& at(const Point<N>& pt);      
	const ElemType& at(const Point<N>& pt) const; 
};*/

#define KD_MAX_POINTS 2000000    //max KD tree points number  
#define SD 3					//current space dimension
#define RMAX 1000
#define POINTS_NUM 10000
#define TEST_NUM 50

#define KDTEMPLATE	template <class Vertex>
#define KDNODE		KDNode<Vertex>
#define KDTREE		KDTree<Vertex>

KDTEMPLATE inline Vertex distance2(Vertex* x1, Vertex* x2, int dim)
{
	Vertex S = 0;
	for (int k = 0; k < dim; k++)
		S += (x1[k] - x2[k])*(x1[k] - x2[k]);
	return S;
}


KDTEMPLATE	bool equal(Vertex* x1, Vertex* x2, int dim)
{
	for (int k = 0; k < dim; k++)
	{
		if (x1[k] != x2[k])
			return false;
	}

	return true;
}

//KDTreeNode template class implementation
KDTEMPLATE class KDNode
{
	//member functions
public:
	int axis;
	Vertex x[SD];
	unsigned int id;
	bool checked;
	bool orientation;

	KDNode(Vertex* x0, int axis0);

	KDNODE*	Insert(Vertex* x);
	KDNODE*	FindParent(Vertex* x0);

	KDNODE* Parent;
	KDNODE* Left;
	KDNODE* Right;
};


KDTEMPLATE
KDNODE::KDNode(Vertex* x0, int axis0)
{
	axis = axis0;
	for (int k = 0; k<SD; k++)
		x[k] = x0[k];

	Left = Right = Parent = NULL;
	checked = false;
	id = 0;
}


KDTEMPLATE
KDNODE*	KDNODE::FindParent(Vertex* x0)
{
	KDNODE* parent;
	KDNODE* next = this;
	int split;
	while (next)
	{
		split = next->axis;
		parent = next;
		if (x0[split] > next->x[split])
			next = next->Right;
		else
			next = next->Left;
	}
	return parent;
}

KDTEMPLATE
KDNODE*	KDNODE::Insert(Vertex* p)
{
	KDNODE* parent = FindParent(p);
	if (equal(p, parent->x, SD))
		return NULL;

	KDNODE* newNode = new KDNODE(p, parent->axis + 1 < SD ? parent->axis + 1 : 0);
	newNode->Parent = parent;

	if (p[parent->axis] > parent->x[parent->axis])
	{
		parent->Right = newNode;
		newNode->orientation = 1;
	}
	else
	{
		parent->Left = newNode;
		newNode->orientation = 0;
	}

	return newNode;
}



KDTEMPLATE
class KDTree
{
public:
	KDNODE*  Root;
	KDTree();

	bool				add(Vertex* x);
	KDNODE*				find_nearest(Vertex* x0);
	KDNODE*				find_nearest_brute(Vertex* x);

	inline	void		check_subtree(KDNODE* node, Vertex* x);
	inline  void		set_bounding_cube(KDNODE* node, Vertex* x);
	inline KDNODE*		search_parent(KDNODE* parent, Vertex* x);
	void				uncheck();

	LARGE_INTEGER		TimeStart, TimeFinish;
	LARGE_INTEGER		CounterFreq;

	void GetTimerFrequency() {
		QueryPerformanceFrequency(&CounterFreq);
	}

	void StartTimer() {
		QueryPerformanceCounter(&TimeStart);
	}

	void StopTimer() {
		QueryPerformanceCounter(&TimeFinish);
	}

	double GetElapsedTime() {
		double calc_time = (double)(TimeFinish.LowPart - TimeStart.LowPart) / (double)CounterFreq.LowPart;
		return 1000 * calc_time;
	}

public:
	Vertex				d_min;
	KDNODE*				nearest_neighbour;

	int					KD_id;

	KDNODE*				List[KD_MAX_POINTS];
	int					nList;

	KDNODE*				CheckedNodes[KD_MAX_POINTS];
	int					checked_nodes;

	Vertex				x_min[SD], x_max[SD];
	bool				max_boundary[SD], min_boundary[SD];
	int					n_boundary;

};

KDTEMPLATE
KDTREE::KDTree()
{
	Root = NULL;
	KD_id = 1;
	nList = 0;
}


KDTEMPLATE
bool KDTree<Vertex>::add(Vertex* x)
{
	if (nList >= KD_MAX_POINTS - 1)
		return 0; //can't add more points

	if (!Root)
	{
		Root = new KDNODE(x, 0);
		Root->id = KD_id++;
		List[nList++] = Root;
	}
	else
	{
		KDNODE* pNode;
		if ((pNode = Root->Insert(x)))
		{
			pNode->id = KD_id++;
			List[nList++] = pNode;
		}
	}

	return true;
}


//sequential nearest neighbour search
KDTEMPLATE
KDNODE* KDTree<Vertex>::find_nearest_brute(Vertex* x)
{
	if (!Root)
		return NULL;

	KDNODE* nearest = Root;
	Vertex d;
	d_min = distance2(Root->x, x, SD);
	for (int n = 1; n<nList; n++)
	{
		d = distance2(List[n]->x, x, SD);
		if (d < d_min)
		{
			nearest = List[n];
			d_min = d;
		}
	}

	return nearest;
}


KDTEMPLATE
KDNODE* KDTREE::find_nearest(Vertex* x)
{
	if (!Root)
		return NULL;

	checked_nodes = 0;

	KDNODE* parent = Root->FindParent(x);
	nearest_neighbour = parent;
	d_min = distance2(x, parent->x, SD);;

	if (equal(x, parent->x, SD))
		return nearest_neighbour;

	search_parent(parent, x);
	uncheck();

	return nearest_neighbour;
}




KDTEMPLATE
void KDTREE::check_subtree(KDNODE* node, Vertex* x)
{
	if (!node || node->checked)
		return;

	CheckedNodes[checked_nodes++] = node;
	node->checked = true;
	set_bounding_cube(node, x);

	int dim = node->axis;
	Vertex d = node->x[dim] - x[dim];

	if (d*d > d_min) {
		if (node->x[dim] > x[dim])
			check_subtree(node->Left, x);
		else
			check_subtree(node->Right, x);
	}
	// If the distance from the key to the current value is 
	// less than the nearest distance, we still need to look
	// in both directions.
	else {
		check_subtree(node->Left, x);
		check_subtree(node->Right, x);
	}
}

KDTEMPLATE
void KDTREE::set_bounding_cube(KDNODE* node, Vertex* x)
{
	if (!node)
		return;
	int d = 0;
	Vertex dx;
	for (int k = 0; k<SD; k++)
	{
		dx = node->x[k] - x[k];
		if (dx > 0)
		{
			dx *= dx;
			if (!max_boundary[k])
			{
				if (dx > x_max[k])
					x_max[k] = dx;
				if (x_max[k]>d_min)
				{
					max_boundary[k] = true;
					n_boundary++;
				}
			}
		}
		else
		{
			dx *= dx;
			if (!min_boundary[k])
			{
				if (dx > x_min[k])
					x_min[k] = dx;
				if (x_min[k]>d_min)
				{
					min_boundary[k] = true;
					n_boundary++;
				}
			}
		}
		d += dx;
		if (d>d_min)
			return;

	}

	if (d<d_min)
	{
		d_min = d;
		nearest_neighbour = node;
	}
}

KDTEMPLATE
KDNODE* KDTREE::search_parent(KDNODE* parent, Vertex* x)
{
	for (int k = 0; k<SD; k++)
	{
		x_min[k] = x_max[k] = 0;
		max_boundary[k] = min_boundary[k] = 0;
	}
	n_boundary = 0;

	Vertex dx;
	KDNODE* search_root = parent;
	while (parent && n_boundary != 2 * SD)
	{
		check_subtree(parent, x);
		search_root = parent;
		parent = parent->Parent;
	}

	return search_root;
}

KDTEMPLATE
void KDTREE::uncheck()
{
	for (int n = 0; n<checked_nodes; n++)
		CheckedNodes[n]->checked = false;
}
