#ifndef RAYTRACING_Hjdslkjfadjfasljf
#define RAYTRACING_Hjdslkjfadjfasljf
#include <vector>
#include "mesh.h"
#include "Model.cpp"


//Welcome to your MIN PROJECT...
//THIS IS THE MOST relevant code for you!
//this is an important file, raytracing.cpp is what you need to fill out
//In principle, you can do the entire project ONLY by working in these two files

extern Mesh MyMesh; //Main mesh
extern std::vector<Vec3Df> MyLightPositions;
extern std::vector<Vec3Df> ray;
extern Vec3Df MyCameraPosition; //currCamera
extern unsigned int WindowSize_X;//window resolution width
extern unsigned int WindowSize_Y;//window resolution height
extern unsigned int RayTracingResolutionX;  // largeur fenetre
extern unsigned int RayTracingResolutionY;  // largeur fenetre

//use this function for any preprocessing of the mesh.
void init();

//you can use this function to transform a click to an origin and destination
//the last two values will be changed. There is no need to define this function.
//it is defined elsewhere
void produceRay(int x_I, int y_I, Vec3Df & origin, Vec3Df & dest);


//your main function to rewrite
Vec3Df performRayTracing(const Vec3Df & origin, const Vec3Df & dest);

//a function to debug --- you can draw in OpenGL here
void yourDebugDraw();

//want keyboard interaction? Here it is...
void yourKeyboardFunc(char t, int x, int y, const Vec3Df & rayOrigin, const Vec3Df & rayDestination);
std::pair<bool,float> intersectTriangle(Triangle& tri, const Vec3Df& o, const Vec3Df& dir);
bool interPlane(float curr_t, const Vec3Df& origin, const Vec3Df dir);
void shade(int level, Vec3Df& inter, Vec3Df& normal, const Vec3Df& origin, const Vec3Df& dir, Material& mat,int currTriangle, Vec3Df& color);
void trace(int level,const Vec3Df& origin,const Vec3Df& dir, Vec3Df& color);
void traceSphere(int level,const Vec3Df& origin,const Vec3Df& dir, Vec3Df& color);
void computeReflectedRay(const Vec3Df& origin,const Vec3Df& dir, Vec3Df& inter, Vec3Df& normal, Vec3Df& reflRay);
Vec3Df getNormalFromTriangle(Triangle& triangle);
bool intersectSphere(Vec3Df& middle, float radius, const Vec3Df& origin, const Vec3Df& dir, Vec3Df& inter);

//void computeRefractedRay(const Vec3Df& origin, const Vec3Df& dir, Vec3Df& normal, Vec3Df& refrRay, Material& mat);
void computeRefractedRay(const Vec3Df &normal, const Vec3Df &dir, Material& mat, Vec3Df & reflRay);

bool pointInTriangle(const Vec3Df& b, const Vec3Df& p0, const Vec3Df& p1, const Vec3Df& p2);
bool isObstructed(Vec3Df& inter,Vec3Df& light,int currTriangle);

#endif
