#include "Vec3D.h"


class KDNode
{
public:
    int axis;        
	Vec3D<float> v[3];        
    unsigned int id ;
    bool checked ;        //flag needed for recursive parent check
	bool orientation;


public:
    KDNode(Vec3D<float>* v0, int split_axis);  //contructor

	KDNode*    Insert(Vec3D<float>* v);
    KDNode*    FindParent(Vec3D<float>* v0);

	KDNode* Parent;
	KDNode* Left;
	KDNode* Right;

	inline float distance(Vec3D<float>* x1, Vec3D<float>* x2, int dim)
	{
		float S = 0;
		for (int k = 0; k < dim; k++)
			S += (x1[0][k] - x2[0][k])*(x1[0][k] - x2[0][k]);
		S = sqrt(S);
		return S;
	}


	bool equal(Vec3D<float>* x1, Vec3D<float>* x2, int dim)
	{
		for (int k = 0; k < dim; k++)
		{
			if (x1[0][k] != x2[0][k])
				return false;
		}

		return true;
	}
};