#include "BBox.h"

// initial BBox is empty => minPoint > maxPoint
BBox::BBox() {
	minPoint.p[0] = 99999;
	minPoint.p[1] = 99999;
	minPoint.p[2] = 99999;

	maxPoint.p[0] = -99999;
	maxPoint.p[1] = -99999;
	maxPoint.p[2] = -99999;
};
// Create BBox from min and max coordinates
BBox::BBox(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) {
	minPoint.p[0] = minX;
	minPoint.p[1] = minY;
	minPoint.p[2] = minZ;

	maxPoint.p[0] = maxX;
	maxPoint.p[1] = maxY;
	maxPoint.p[2] = maxZ;
};
// Create BBox from min and max points
BBox::BBox(const Vertex & minP, const Vertex & maxP) {
	minPoint.p[0] = minP.p[0];
	minPoint.p[1] = minP.p[1];
	minPoint.p[2] = minP.p[2];

	maxPoint.p[0] = maxP.p[0];
	maxPoint.p[1] = maxP.p[1];
	maxPoint.p[2] = maxP.p[2];
};

// BBox is empty if coordinates of min point are greater than coordinates of max point
bool BBox::IsEmpty()
{
	for (int i = 0; i<3; i++)
	{
		if (minPoint.p[i] >= maxPoint.p[i])
		{
			return true;
		}
	}

	return false;
}

Vertex BBox::getMinPoint() {
	return minPoint;
}

Vertex BBox::getMaxPoint() {
	return maxPoint;
}

// Check if ray intersects BBox
bool BBox::intersect(const Vec3Df& origin, const Vec3Df& dir, float tMax) {
	if (IsEmpty()) return false;

	return true;
}
