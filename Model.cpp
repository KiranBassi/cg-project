#include "Vec3D.h"
#include "mesh.h"

class Model{

	public: 
		virtual bool intersect(Vec3Df& origin, Vec3Df& dir, Vec3Df& inter, Vec3Df& normal, float& t) = 0;

};

/*bool interPlane(float curr_t, const Vec3Df& origin, const Vec3Df dir){
	Triangle& triangle = MyMesh.triangles[curr_t];
    Vec3Df normal = getNormalFromTriangle(triangle);
    Vec3Df& point = MyMesh.vertices[triangle.v[0]].p;
	Vec3Df ver[3];

    for(int j = 0; j < 3; j++)
    {
        ver[j] = MyMesh.vertices[triangle.v[j]].p;
		if (Vec3Df::dotProduct(normal, ver[j]) < 0)
        {
            normal = normal * -1;
        }
    }
    
	Vec3Df dest = origin.Vec3Df::projectOn(normal, point);
	float d = dest.getLength();
	float t = (d - Vec3Df::dotProduct(origin, normal)) / Vec3Df::dotProduct(dir, normal);
	if (Vec3Df::dotProduct(point, normal) - d == 0)
		return true;
	return false;
    
    // for(int j = 0; j < 3; j++) ver[j] = MyMesh.vertices[triangle.v[j]].p;
    // Vec3Df a = v[0] - v[2];
    // Vec3Df b = v[1] - v[2];
    
    // Vec3Df cross = crossProduct(a, b);
    
    // float normal = cross / cross.normalize();
}*/
/*void testTrace(){
	//Find closest triangle which intersects with the ray
	Vec3Df dir = testRayDestination - testRayOrigin;
	float t = 0;
	int currTriangle;
	Vec3Df currDir(0,0,0);
	Vec3Df inter(0,0,0);
	Vec3Df normal(0,0,0);
	Material mat;

	if(!intersect(testRayOrigin,dir,inter,normal,mat)){
		return;
	}
	
	//Get material of intersecting triangle !!!!!!!!!!!!!
	//See Matrial class in mesh.h file
	//unsigned int materialIndex = MyMesh.triangleMaterials[currTriangle];
	//Material& mat = MyMesh.materials[materialIndex];

	//Vec3Df inter = testRayOrigin + t * dir;
	
	/ge/Get normal of intersection Triangle
	Triangle& triangle = MyMesh.triangles[currTriangle];
	Vec3Df n = getNormalFromTriangle(triangle);

	//light vector
	Vec3Df l = inter - testRayOrigin;
	Vec3Df refl = reflVector(n,l);
	Vec3Df newpoint = inter + refl*100;

	ray.push_back(inter);
	ray.push_back(newpoint);
	
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_LIGHTING);
	
	//Draw intersection point
	glPointSize(10.0f);
	glBegin( GL_POINTS);
	glColor3f(0,1,0);
	glVertex3f(inter[0],inter[1],inter[2]);
	glEnd();

	//Draw incoming ray as a red line
	glBegin(GL_LINES);
	glColor3f(1,0,0);
	glVertex3f(inter[0], inter[1], inter[2]);
	glColor3f(1,0,0);
	glVertex3f(testRayOrigin[0], testRayOrigin[1], testRayOrigin[2]);
	glEnd();
	glPopAttrib();

	//std::cout << "count: " << count << std::endl;
}*/
