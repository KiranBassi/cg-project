#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#endif

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#else
#include <GL/glut.h>
#endif

#include "Vec3D.h"
#include <list>
#include <set>
#include <vector>
#include <math.h>
#include <cmath>

#define EPSILON 0.00001
#define FLT_MAX 1E10F
#define MATRIX_SIZE 3
#define maxLevel 3

inline float minX(float a, float b)
{
	return (a < b ? a : b);
}
inline float minX(float a, float b, float c)
{
	return (a < b ? (a < c ? a : c) : (b < c ? b : c));
}

inline float maxX(float a, float b)
{
	return (a > b ? a : b);
}
inline float maxX(float a, float b, float c)
{
	return (a > b ? (a > c ? a : c) : (b > c ? b : c));
}
/*bool intersectSphere(Vec3Df& middle, float radius, Vec3Df& origin, Vec3Df& dir, float& t0, float& t1) {
	Vec3Df res;

	Vec3Df newori = origin - middle;

	float A, B, C;
	A = dir[0] * dir[0] + dir[1] * dir[1] + dir[2] * dir[2];
	B = 2*(dir[0] * newori[0] + dir[1] * newori[1] + dir[2] * newori[2]);
	C = newori[0] * newori[0] + newori[1] * newori[1] + newori[2] * newori[2] - radius * radius;

	float D = B * B - 4 * A * C;

	if (D < 0) {
		return false;
	}

	t0 = (-B + sqrt(D)) / (2 * A);
	t1 = (-B - sqrt(D)) / (2 * A);

	return true;
}*/
	/*Material mat;
	mat.set_Kd(1.0,0.f,0.4); //Color of material
	mat.set_Ka(0.3,0.3,0.3); //Ambiance
	mat.set_Ks(0.9,0.9,0.9); //Specular
	mat.set_Ns(20.0); //Shininess
	mat.set_Ni(5.0);
	mat.set_Tr(1.f); //transparancy

	Vec3Df middle1(0,0,0);
	Vec3Df middle2(3,3,3);
	float radius = 1.0f;
	SphereModel *sphere1 = new SphereModel(radius,middle1,&mat);
	SphereModel *sphere2 = new SphereModel(radius,middle2,&mat);

	objects.push_back(sphere1);
	objects.push_back(sphere2);*/

bool intersectBox(std::pair<Vec3Df, Vec3Df> box, Vec3Df& origin, Vec3Df& dir, float& tin, float& tout) {
	Vec3Df bmin = box.first;
	Vec3Df bmax = box.second;

	// calculate minimum and maximum
	float xmin, xmax, ymin, ymax, zmin, zmax;
	xmin = bmin[0];
	xmax = bmax[0];
	ymin = bmin[1];
	ymax = bmax[1];
	zmin = bmin[2];
	zmax = bmax[2];

	// calculate intersection parameter
	float txmin, txmax, tymin, tymax, tzmin, tzmax;
	txmin = (xmin - origin[0]) / (dir[0]);
	txmax = (xmax - origin[0]) / (dir[0]);
	tymin = (ymin - origin[1]) / (dir[1]);
	tymax = (ymax - origin[1]) / (dir[1]);
	tzmin = (zmin - origin[2]) / (dir[2]);
	tzmax = (zmax - origin[2]) / (dir[2]);

	// calculate entry and exit points
	float tinx, toutx, tiny, touty, tinz, toutz;
	tinx = minX(txmin, txmax);
	toutx = maxX(txmin, txmax);
	tiny = minX(tymin, tymax);
	touty = maxX(tymin, tymax);
	tinz = minX(tzmin, tzmax);
	toutz = maxX(tzmin, tzmax);

	// find global entry and exit
	tin = maxX(tinx, tiny, tinz);
	tout = minX(toutx, touty, toutz);

	// check if there is a hit
	if (tin > tout | tout < 0) {
		return false;
	}

	return true;
}

