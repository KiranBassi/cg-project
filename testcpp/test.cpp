#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include "Vec3D.h"
#include <list>
#include <set>
#include <vector>
#include <math.h>

using namespace std;
#define EPSILON 1e-7


std::pair<bool,float> solveMatrix(Vec3Df v0, Vec3Df v1, Vec3Df v2, Vec3Df o, Vec3Df dir)
{
		Vec3Df _u = v1 - v0;
		Vec3Df _v = v2 - v0;

		cout << "u: " << _u << endl;
		cout << "v: " << _v << endl;
		
		float a,b,c,d,e,f,g,h,_i;
		a = _u[0], b = _v[0], c = dir[0];
		d = _u[1], e = _v[1], f = dir[1];
		g = _u[2], h = _v[2], _i = dir[2];

		float A,B,C,D,E,F,G,H,I;
		A = e*_i-f*h;
		B = c*h-b*_i;
		C = b*f-c*e;
		D = f*g-d*_i;
		E = a*_i-c*g;
		F = c*d-a*f;
		G = d*h-e*g;
		H = b*g-a*h;
		I = a*e-b*d;

		float det = a*(e*_i-f*h)-b*(d*_i-f*g)+c*(d*h-e*g);
		cout << "det: " << det << endl;
		det = -det;

		if(det < EPSILON && det > -EPSILON)
			return std::pair<bool,float>(false,0);;

		A /= det;
		B /= det;
		C /= det;
		D /= det;
		E /= det;
		F /= det;
		G /= det;
		H /= det;
		I /= det;

		Vec3Df r = o - v0;
		cout << "r:" << r << endl;
		float alpha = A*r[0] + B*r[1] + C*r[2];
		float beta = D*r[0] + E*r[1] + F*r[2];
		float t = G*r[0] + H*r[1] + I*r[2];

		cout << "alpha: " << alpha << endl;
		cout << "beta: " << beta << endl;
		cout << "t: " << t << endl;

		if(alpha < -1 || alpha > 1 || beta > 1 || beta < -1)
			return std::pair<bool,float>(false,0);
		
		return std::pair<bool,float>(true,t);;
}

Vec3Df reflVector(Vec3Df n,Vec3Df l)
{
	n.normalize();
	return l - 2 * Vec3D<float>::dotProduct(l,n) * n;
}

int main()
{
	//Vec3Df v0(3,3,3);
	//Vec3Df v1(4,2,3); //1 -1 0
	//Vec3Df v2(4,5,0); //1 2 -3
	//Vec3Df o(1,1,1);
	//Vec3Df dir(1,2,1);
	Vec3Df v0(-1,3,0);
	Vec3Df v1(1,3,0); //1 -1 0
	Vec3Df v2(0,3,1); //1 2 -3
	Vec3Df o(0,0,0);
	Vec3Df dir(0,1,0);

	std::pair<bool,float> res = solveMatrix(v0,v1,v2,o,dir);
	cout << "hit: " << res.first << endl;
	cout << "t: " << res.second << endl;
	Vec3Df inter = o + res.second*dir;
	cout << "inter: " << inter << endl;

	Vec3Df n(0,0,1);
	Vec3Df l(-1,-1,-1);
	cout << "refl: " << reflVector(n,l);

	return 0;
}

