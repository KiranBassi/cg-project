#include "Vec3D.h"
#include "mesh.h"
#include "Model.cpp"

#define EPSILON 0.00001
#define FLT_MAX 1E10F
#define MATRIX_SIZE 3
#define maxLevel 3


class BoxModel: public Model{
	public:
		BoxModel()
		{
			tin = 0;
			tout = 0;
			//box = std::make_pair(Vec3Df(0,0,0), Vec3Df(0,0,0));
			a = 0;
			b = 0;
			c = 0;
		};

		BoxModel(std::pair<Vec3Df, Vec3Df> box, float& tin, float& tout)
		{
			this->tin = tin;
			this->tout = tout;
			this->box = box;

		}

		float getTin(){
			return tin;
		}

		float getTout(){
			return tout;
		}

		std::pair<Vec3Df, Vec3Df> getBox(){
			return box;
		}

		inline float minX(float a, float b){
			return (a < b ? a : b);
		}

		inline float minX(float a, float b, float c){
			return (a < b ? (a < c ? a : c) : (b < c ? b : c));
		}

		inline float maxX(float a, float b)
		{
		return (a > b ? a : b);
		}

		inline float maxX(float a, float b, float c)
		{
		return (a > b ? (a > c ? a : c) : (b > c ? b : c));
		}

		bool intersect(Vec3Df& origin, Vec3Df& dir, Vec3Df& inter, Vec3Df& normal, float& t) {
			Vec3Df bmin = box.first;
			Vec3Df bmax = box.second;

			// calculate minimum and maximum
			float xmin, xmax, ymin, ymax, zmin, zmax;
			xmin = bmin[0];
			xmax = bmax[0];
			ymin = bmin[1];
			ymax = bmax[1];
			zmin = bmin[2];
			zmax = bmax[2];

			// calculate intersection parameter
			float txmin, txmax, tymin, tymax, tzmin, tzmax;
			txmin = (xmin - origin[0]) / (dir[0]);
			txmax = (xmax - origin[0]) / (dir[0]);
			tymin = (ymin - origin[1]) / (dir[1]);
			tymax = (ymax - origin[1]) / (dir[1]);
			tzmin = (zmin - origin[2]) / (dir[2]);
			tzmax = (zmax - origin[2]) / (dir[2]);

			// calculate entry and exit points
			float tinx, toutx, tiny, touty, tinz, toutz;
			tinx = minX(txmin, txmax);
			toutx = maxX(txmin, txmax);
			tiny = minX(tymin, tymax);
			touty = maxX(tymin, tymax);
			tinz = minX(tzmin, tzmax);
			toutz = maxX(tzmin, tzmax);

			// find global entry and exit
			tin = maxX(tinx, tiny, tinz);
			tout = minX(toutx, touty, toutz);

			// check if there is a hit
			if (tin > tout | tout < 0) {
				return false;
			}

			return true;
		}


	private:
		float tin;
		float tout;
		std::pair<Vec3Df, Vec3Df> box;
		float a;
		float b;
		float c;
};
