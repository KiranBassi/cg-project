#pragma once
#include "mesh.h"

// class Boundary Box
class BBox
{
private:
	Vertex minPoint;
	Vertex maxPoint;
public:
	BBox();
	BBox(float minX, float minY, float minZ, float maxX, float maxY, float maxZ);
	BBox(const Vertex & iMin, const Vertex & iMax);

	Vertex getMinPoint();
	Vertex getMaxPoint();
	bool IsEmpty();
	bool intersect(const Vec3Df& origin, const Vec3Df& dir, float tMax);
	//bool Intersect(const Ray & r, float & t_max) const;
}; 