#include "KDNode.h"


//constructor
KDNode::KDNode(Vec3D<float>* v0, int axis0)
{
	axis = axis0;
	for (int k = 0; k<3; k++)
		v[k] = v0[k];

	Left = Right = Parent = NULL;
	checked = false;
	id = 0;
}

KDNode* KDNode::FindParent(Vec3D<float>* v0)
{
	KDNode* next = this;
	KDNode* parent = next;
	int split;
	while (next)
	{
		split = next->axis;
		parent = next;
		if (v0[split] > next->v[split])
			next = next->Right;
		else
			next = next->Left;
	}
	return parent;
}

KDNode*	KDNode::Insert(Vec3D<float>* p)
{
	KDNode* parent = FindParent(p);
	if (equal(p, parent->v, 3))
		return NULL;

	KDNode* newNode = new KDNode(p, parent->axis + 1 < 3 ? parent->axis + 1 : 0);
	newNode->Parent = parent;

	if (p[parent->axis] > parent->v[parent->axis])
	{
		parent->Right = newNode;
		newNode->orientation = 1;
	}
	else
	{
		parent->Left = newNode;
		newNode->orientation = 0;
	}

	return newNode;
}