#include <stdio.h>
#ifdef _WIN32
#include <windows.h>
#endif
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#else
#include <GL/glut.h>
#endif
#include "raytracing.h"
#include <list>
#include <set>
#include <vector>
#include <math.h>
#include "sphereModel.cpp"
#define EPSILON 0.00001
#define FLT_MAX 1E10F
#define MATRIX_SIZE 3
#define maxLevel 6 //Maximum level of recursion

Vec3Df testRayOrigin;
Vec3Df testRayDestination;
inline Vec3Df reflVector(Vec3Df n,Vec3Df l);
using namespace std;
//use this function for any preprocessing of the mesh.
void init()
{
    MyMesh.loadMesh("lighten-ballsWithoutTexture.obj", true);
	MyMesh.computeVertexNormals();
	MyLightPositions.push_back(MyCameraPosition);
}

//return the color of your pixel.
Vec3Df performRayTracing(const Vec3Df & origin, const Vec3Df & dest)
{
	Vec3Df dir = dest - origin;
	Vec3Df color(0,0,0);
	trace(0,origin,dir,color);
	return color;
}

/*
 * Find closest triangle which intersects with the ray
 * and return intersection point, normal of the triangle on the intersection point 
 * and Material object of the traingle
 */
bool intersect(const Vec3Df& origin, const Vec3Df& dir,Vec3Df& inter,Vec3Df& normal,Material& mat, int& currTriangle)
{
	std::vector<Triangle>& tri = MyMesh.triangles;
	currTriangle = -1;
	float curr_t = FLT_MAX;
	float t = 0;	
	bool intersection = false;
	for(int i = 0; i < tri.size(); i++){
		std::pair<bool,float> inter = intersectTriangle(tri[i],origin, dir);
		if(!inter.first) continue; //When there is no intersection with triangle tr[i], continue
		t = inter.second;
		if (t < curr_t){
			curr_t = t;
			currTriangle = i;
			intersection = true;
		}
	}
	if(!intersection) return false;
	inter = origin + curr_t * dir;
	unsigned int materialIndex = MyMesh.triangleMaterials[currTriangle];
	mat = MyMesh.materials[materialIndex];
	normal = getNormalFromTriangle(tri[currTriangle]);
	if(Vec3Df::dotProduct(normal,MyMesh.vertices[tri[0].v[0]].n) < 0) normal = -normal;
	normal.normalize();
	return true;
}

/**
 * Calculate intersection between Triangle t and ray: p=o+t*dir
 * Using Moller-Trumbore algorithm
 * https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
 */
inline std::pair<bool,float> intersectTriangle(Triangle& tri, const Vec3Df& o, const Vec3Df& dir) {
	Vec3Df& p1 = MyMesh.vertices[tri.v[0]].p;
	Vec3Df& p2 = MyMesh.vertices[tri.v[1]].p;
	Vec3Df& p3 = MyMesh.vertices[tri.v[2]].p;
	Vec3Df e1,e2,P,Q,T;
	float det,u,v,t;
	e1 = p2 - p1;
	e2 = p3 - p1;
	P = Vec3Df::crossProduct(dir,e2);
	det = Vec3Df::dotProduct(e1,P);
	if(det > -EPSILON && det < EPSILON) return std::pair<bool,float>(false,0);
	T = o - p1;
	u = Vec3Df::dotProduct(T,P) / det;
	if(u < 0.f || u > 1.f) return std::pair<bool,float>(false,0);
	Q = Vec3Df::crossProduct(T,e1);
	v = Vec3Df::dotProduct(dir,Q) / det;
	if(v < 0.f || u + v > 1.f) return std::pair<bool,float>(false,0);
	t = Vec3Df::dotProduct(e2,Q) / det;
	if(t > EPSILON) return std::pair<bool,float>(true,t);
	return std::pair<bool,float>(false,0);
}

void trace(int level,const Vec3Df& origin,const Vec3Df& dir, Vec3Df& color)
{
	float t = 0;
	int currTriangle;
	Vec3Df currDir(0,0,0), inter(0,0,0), normal(0,0,0);
	Material mat;

	if(intersect(origin, dir, inter,normal, mat, currTriangle))
	{
		shade(level,inter,normal,origin,dir,mat,currTriangle, color);
	}
	else
	{
		color.init(0,0,0);
	}
}

void shade(int level, Vec3Df& inter, Vec3Df& normal, const Vec3Df& origin, const Vec3Df& dir, Material& mat,int currTriangle, Vec3Df& color)
{
	Vec3Df directColor(0,0,0),reflectedColor(0,0,0), refractedColor(0,0,0);
	Vec3Df reflRay(0,0,0), refrRay(0,0,0), I(1,1,1);
	//Light intensity
	I *= 0.5f;
	I /= MyLightPositions.size();
	//Compute direct light
	for(int i = 0; i < MyLightPositions.size(); i++)
	{
		if (isObstructed(inter,MyLightPositions[i], currTriangle)) continue;
		Vec3Df A(0,0,0), D(0,0,0), S(0,0,0);
		Vec3Df lightDir = MyLightPositions[i] - inter;
		lightDir.normalize();
		//Ambiance Term
		if (mat.has_Kd()){
			A = I * mat.Kd(); 
			directColor += A;
		}
		//Diffuse term
		if ( mat.has_Kd()){
			float cos_theta = Vec3Df::dotProduct(lightDir,normal);
			if (cos_theta > 0.0f){
				D = I * mat.Kd() * cos_theta;
				directColor += D;
			}
			if (cos_theta < 0.0f){
				D = - I * mat.Kd() * cos_theta;
				directColor += D;
			}
		}
		//Blinn Phong specular term
		if ( mat.has_Ks()){
			Vec3Df light = inter - MyLightPositions[i];
			Vec3Df v = MyCameraPosition - inter;
			light.normalize();
			v.normalize();
			Vec3Df h = v - light;
			h.normalize();
			float reflectFactor = Vec3Df::dotProduct(h,normal);
			if(Vec3Df::dotProduct(light,normal) > 0 || reflectFactor < 0) S = Vec3Df(0,0,0);
			else S = I * mat.Ks() * pow(reflectFactor,mat.Ns());
			directColor += S;
		}
	}
	//Compute reflecting color
	if(mat.has_Ks() && mat.Ks() > Vec3Df(0,0,0) && (level < maxLevel))
	{
		computeReflectedRay(origin,dir,inter,normal,reflRay);
		trace(level+1,inter,reflRay,reflectedColor);
	}
	//Compute refracting color
	if(mat.has_Ni() && mat.has_Tr() && mat.Tr() > 0 && (level < maxLevel))
	{
		//computeRefractedRay(origin, dir, normal, refrRay, mat);
		computeRefractedRay(normal, dir, mat, refrRay);
		trace(level+1,inter+EPSILON * refrRay, refrRay, refractedColor);
	}
	color = directColor +
			mat.Ks() * reflectedColor +
			(Vec3Df(1.0f, 1.0f, 1.0f) - mat.Ks()) * (1.0 - mat.Tr()) * refractedColor;
}

inline bool isObstructed(Vec3Df& inter,Vec3Df& light,int thisTriangle)
{
	Vec3Df dir = light - inter, o = inter;
	Vec3Df newinter(0,0,0), newnormal(0,0,0);
	std::vector<Triangle>& tri = MyMesh.triangles;
	int currTriangle = -1;
	float curr_t = 1, t = 0;	
	bool intersection = false;
	for(int i = 0; i < tri.size(); i++){
		std::pair<bool,float> inter = intersectTriangle(tri[i],o, dir);
		if(!inter.first) continue;
		t = inter.second;
		if (t < curr_t && i != thisTriangle){
			curr_t = t;
			currTriangle = i;
			intersection = true;
		}
	}
	if(!intersection){
		return false;
	}
	return true;
}

inline void computeReflectedRay(const Vec3Df& origin, const Vec3Df& dir, Vec3Df& inter, Vec3Df& normal, Vec3Df& reflRay)
{
	Vec3Df d = dir;
	d.normalize();
	reflRay =  d - 2 * Vec3Df::dotProduct(d,normal) * normal;
}

inline Vec3Df getNormalFromTriangle(Triangle& triangle)
{
	Vec3Df a = MyMesh.vertices[triangle.v[1]].p - MyMesh.vertices[triangle.v[0]].p;
	Vec3Df b = MyMesh.vertices[triangle.v[2]].p - MyMesh.vertices[triangle.v[0]].p;
	Vec3Df n = Vec3Df::crossProduct(a,b);
	n.normalize();
	return n;
}

inline void computeRefractedRay(const Vec3Df &normal, const Vec3Df &dir, Material& mat, Vec3Df & reflRay) {
	if (mat.has_Ni()) {
		float dot = Vec3Df::dotProduct(normal, dir);
		float n1, n2;
		Vec3Df newNormal = normal;

		// from object into air
		if (dot > 0) {
			n1 = mat.Ni();
			n2 = 1.00;
			newNormal = -normal;

			// calculate fresnel
			float fzero = pow(((n1 - n2) / (n1 + n2)), 2);
			float fresnel = (fzero + (1 - fzero) * pow((1 - dot), 5)) / 100;
		}
		// from air into object
		else {
			n1 = 1.00;
			n2 = mat.Ni();
			dot = Vec3Df::dotProduct(newNormal, dir);


			float fzero = pow(((n1 - n2) / (n1 + n2)), 2);
			float fresnel = (fzero + (1 - fzero) * pow((1 - dot), 5)) / 100;
		}

		// Calculate the refracted ray
		double n = n1 / n2;
		double root = 1.0 - n * n * (1.0 - dot * dot);

		if (root < 0) {
			reflRay = dir - 2 * dot * newNormal; 
		}
		reflRay = dir * n + (n * dot - sqrt(root)) * newNormal;
	}
}

bool interPlane(int& curr_t, const Vec3Df& origin, const Vec3Df dir){
	Triangle& triangle = MyMesh.triangles[curr_t];
    Vec3Df normal = getNormalFromTriangle(triangle);
    Vec3Df& point = MyMesh.vertices[triangle.v[0]].p;
	Vec3Df ver[3];

    for(int j = 0; j < 3; j++)
    {
        ver[j] = MyMesh.vertices[triangle.v[j]].p;
		if (Vec3Df::dotProduct(normal, ver[j]) < 0)
        {
            normal = normal * -1;
        }
    }
    
	Vec3Df dest = origin.Vec3Df::projectOn(normal, point);
	float d = dest.getLength();
	float t = (d - Vec3Df::dotProduct(origin, normal)) / Vec3Df::dotProduct(dir, normal);
	if (Vec3Df::dotProduct(point, normal) - d == 0)
		return true;
	return false;
}

void yourDebugDraw()
{
	//draw open gl debug stuff
	//this function is called every frame
	//let's draw the mesh
	MyMesh.draw();
	//let's draw the lights in the scene as points
	glPushAttrib(GL_ALL_ATTRIB_BITS); //store all GL attributes
	glDisable(GL_LIGHTING);
	glColor3f(1,1,1);
	glPointSize(20);
	glBegin(GL_POINTS);
	for (int i=0;i<MyLightPositions.size();++i)
		glVertex3fv(MyLightPositions[i].pointer());
	glEnd();
	glPopAttrib();//restore all GL attributes
	//The Attrib commands maintain the state. 
	//e.g., even though inside the two calls, we set
	//the color to white, it will be reset to the previous 
	//state after the pop.
	//as an example: we draw the test ray, which is set by the keyboard function
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	/*for(int i = 0; i < ray.size()-1; i++){
		glColor3f(0,1,1);
		glVertex3f(ray[i][0], ray[i][1], ray[i][2]);
		glColor3f(0,0,1);
		glVertex3f(ray[i+1][0], ray[i+1][1], ray[i+1][2]);
	}*/
	glEnd();
	glPointSize(10);
	glBegin(GL_POINTS);
	glVertex3fv(MyLightPositions[0].pointer());
	glEnd();
	glPopAttrib();
	//draw whatever else you want...
	//glutSolidSphere(1,10,10);
	////allows you to draw a sphere at the origin.
	////using a glTranslate, it can be shifted to whereever you want
	////if you produce a sphere renderer, this 
	////triangulated sphere is nice for the preview
}


//yourKeyboardFunc is used to deal with keyboard input.
//t is the character that was pressed
//x,y is the mouse position in pixels
//rayOrigin, rayDestination is the ray that is going in the view direction UNDERNEATH your mouse position.
//
//A few keys are already reserved: 
//'L' adds a light positioned at the camera location to the MyLightPositions vector
//'l' modifies the last added light to the current 
//    camera position (by default, there is only one light, so move it with l)
//    ATTENTION These lights do NOT affect the real-time rendering. 
//    You should use them for the raytracing.
//'r' calls the function performRaytracing on EVERY pixel, using the correct associated ray. 
//    It then stores the result in an image "result.ppm".
//    Initially, this function is fast (performRaytracing simply returns 
//    the target of the ray - see the code above), but once you replaced 
//    this function and raytracing is in place, it might take a 
//    while to complete...
void yourKeyboardFunc(char t, int x, int y, const Vec3Df & rayOrigin, const Vec3Df & rayDestination)
{
	//here, as an example, I use the ray to fill in the values for my upper global ray variable
	//I use these variables in the debugDraw function to draw the corresponding ray.
	//try it: Press a key, move the camera, see the ray that was launched as a line.
	testRayOrigin=rayOrigin;	
	testRayDestination=rayDestination;
	// do here, whatever you want with the keyboard input t.
	//...
	if(t=='t'){
		Vec3Df color = performRayTracing(testRayOrigin,testRayDestination);
		std::cout << color << std::endl;
	}	
}
