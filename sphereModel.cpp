#include "Vec3D.h"
#include "mesh.h"
#include "raytracing.h"

//Should extend from super class Model
class SphereModel: public Model{
	public:
		SphereModel(){
			radius = 0;
			middle = Vec3Df(0,0,0);
			mat = nullptr;
		};

		SphereModel(float radius, Vec3Df middle, Material * mat){
			this->radius = radius;
			this->middle = middle;
			this->mat = mat;
		}
		float getRadius(){
			return radius;
		}
		Vec3Df getMiddle(){
			return middle;
		}
		Material * getMaterial(){
			return mat;
		}

		bool intersect(Vec3Df& origin, Vec3Df& dir, Vec3Df& inter, Vec3Df& normal, float& t){
			Vec3Df newori = origin - middle;

			float A, B, C,t0,t1;

			A = dir.getSquaredLength();
			B = 2 * Vec3Df::dotProduct(dir,newori);
			C = newori.getSquaredLength() - radius * radius;

			float D = B * B - 4 * A * C;

			if (D < 0) {
				return false;
			}

			t0 = (-B - sqrt(D)) / (2 * A);
			t1 = (-B + sqrt(D)) / (2 * A);

			inter = origin + t0 * dir;
			normal = inter - middle;
			normal.normalize();
			t = t0;

			return true;
		}
				
	private:
		Material * mat;
		Vec3Df middle;
		float radius;
};

/*void traceSphere(int level,const Vec3Df& origin,const Vec3Df& dir, Vec3Df& color)
{
	Vec3Df middle(0,0,0);
	float radius = 1;
	Vec3Df inter(0,0,0);
	Material mat;

	//mat.set_Kd(1.0,0.f,0.9); //Color of material
	mat.set_Kd(color_sphere[0],color_sphere[1],color_sphere[2]);
	mat.set_Ka(0.3,0.3,0.3); //Ambiance
	mat.set_Ks(0.9,0.9,0.9); //Specular
	mat.set_Ns(20.0); //Shininess
	mat.set_Ni(5.0);
	mat.set_Tr(1.f); //transparancy

	Vec3Df normal(0,0,0);

	//for(int

	//if(intersect(origin, dir, inter,normal, mat))
	if(intersectSphere(middle,radius,origin,dir,inter,normal))
	{
		shade(level,inter,normal,origin,dir,mat,color);
	}
	else
	{
		//Set color to background color
		color.init(0,0,0);
	}
}*/

/*bool intersectPlane(const Vec3Df&  origin, const Vec3Df&  direction, Vec3Df&  ray, Triangle triangle) {
    //Triangle tr = (Triangle) triangle;
    //computation of normal vector n
    Vec3Df dot1 = (MyMesh.vertices[triangle.v[0]].p - MyMesh.vertices[triangle.v[2]].p);
    Vec3Df dot2 = (MyMesh.vertices[triangle.v[1]].p - MyMesh.vertices[triangle.v[2]].p);
    Vec3Df n = Vec3Df::crossProduct(dot1, dot2);
    //dot products
    float dota = Vec3Df::dotProduct(origin, n);
    float dotb = Vec3Df::dotProduct(direction, n);
    //normal distance from origin (0,0,0) D
    float D = Vec3Df::distance(origin, n);
    //hit parameter
    float t = (D - dota) / dotb;
    //compute intersection point with plane
    Vec3Df p = origin + (t * direction);
    //test if point is inside triangle with barymetrics
    return false;
}*/
/*bool traceObjects(int level, Vec3Df& origin, Vec3Df& dir, Vec3Df& color)
{
	float curr_t = FLT_MAX;
	Model * currObject = nullptr;
	Vec3Df inter(0,0,0);
	Vec3Df normal(0,0,0);
	float t = 0;
	bool intersection = false;

	for(int i = 0; i < objects.size(); i++){
		if(objects[i]->intersect(origin,dir,inter,normal, t)){
			if(t < curr_t){
				curr_t = t;
				currObject = objects[i];
				intersection = true;
			}
		}
	}

	if(!intersection) return false;
	
	//shade object
	
	return true;
}*/
