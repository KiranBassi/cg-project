#include "Vec3D.h"
#include "mesh.h"

#include "raytracing.h"

#define EPSILON 0.00001
#define FLT_MAX 1E10F
#define MATRIX_SIZE 3
#define maxLevel 3



class TriangleModel: public Model{
	public:
		TriangleModel()
		{

		};

		TriangleModel(Triangle& tri)
		{
			this->tri = tri;
		}

		Triangle getTriangle(){
			return tri;
		}

		inline bool intersect(Vec3Df& origin, Vec3Df& dir, Vec3Df& inter, Vec3Df& normal, float& t){
			Vec3Df ver[3];
			for(int j = 0; j < 3; j++) ver[j] = MyMesh.vertices[tri.v[j]].p;
			Vec3Df _u = ver[1] - ver[0];
			Vec3Df _v = ver[2] - ver[0];
			
			float a,b,c,d,e,f,g,h,i,A,B,C,D,E,F,G,H,I;
			a = _u[0], b = _v[0], c = -dir[0];
			d = _u[1], e = _v[1], f = -dir[1];
			g = _u[2], h = _v[2], i = -dir[2];

			A = e*i-f*h, B = c*h-b*i, C = b*f-c*e;
			D = f*g-d*i, E = a*i-c*g, F = c*d-a*f;
			G = d*h-e*g, H = b*g-a*h, I = a*e-b*d;

			float det = a*A-b*(-D)+c*G;
			//If det is 0, the system has no solution which means there is no intersection
			if(det < EPSILON && det > -EPSILON)
			{	return false;
			}

			A /= det, B /= det, C /= det;
			D /= det, E /= det, F /= det;
			G /= det, H /= det, I /= det;

			Vec3Df r = origin - ver[0];
			float alpha = A*r[0] + B*r[1] + C*r[2];
			float beta = D*r[0] + E*r[1] + F*r[2];
			t = G*r[0] + H*r[1] + I*r[2];

			if(alpha < 0|| alpha > 1 || beta > 1 || beta < 0 || alpha + beta > 1)
			{
				return false;
			}	

			return true;
			
		}

	private:
		Triangle tri;

};

